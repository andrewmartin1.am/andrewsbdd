# AndrewsBDD

## Heading 2

### Heading 3

**This text is Bold**

_This text is italic_

`document.querySelector()`
```javascript

function nagraStreamLoad(){
    let streamInputField = "input#stream-url-input"
    let loadButton = "button#stream-load-btn"
    document.querySelector(streamInputField).value="https://otvplayer.nagra.com/demo/content/ed_tears_of_steel_1080p/tears_of_steel.mpd"
    document.querySelector(loadButton).click()
} 
```
[This is a link to the bbc](https://www.bbc.co.uk/)
