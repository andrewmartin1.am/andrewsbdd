const { Before, When, Then } = require("cucumber");
const { expect } = require("chai");
const { By } = require("selenium-webdriver");

When("the user enters {int}", async function(userInput) {
  //we need to enter the number into the input field
  await this.browser.findElement(By.css("input#userInput")).sendKeys(userInput);

  //we need to click on 'submit'
  return await this.browser.findElement(By.css("input#submit")).click();
});

Then("the word {string} is returned", async function(expectedResult) {
  //Find what result is displayed
  let result = await this.browser.findElement(By.css("h1#result")).getText();

  //check it against the expected result
  return expect(result).to.equal(expectedResult);
});
