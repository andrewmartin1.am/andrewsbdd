const { After, Before } = require("cucumber");

//This is used to connect to our driver application
const driver = require("selenium-webdriver");

let { setDefaultTimeout } = require("cucumber");

setDefaultTimeout(60 * 1000);

Before(async function() {
  this.browser = new driver.Builder()
    //This is where we choose the browser
    .forBrowser("firefox")
    //This connects to the server that we started with 'npm run grid'
    .usingServer("http://localhost:4444/wd/hub")
    //This sets it all up
    .build();

  //This opens the browser and navigates to our website
  return await this.browser.get("http://localhost:52330/src/index.html");
});

After(async function() {
  // This closes our browser when the test is finished - we1ll move this to an `After` latwer
  return await this.browser.quit();
});
