Feature: FizzBuzz

    Acceptance Criteria:

    1. Number is divisable by 3, return Fizz
    2. Number is divisable by 5 , return Buzz
    3. Number is divisable by 3 and 5 , return FizzBuzz
    4. Any other number, return the number


    @e2e
    @debug
    Scenario Outline:: Number <input> is divisable by 3, return Fizz
        When the user enters <input>
        Then the word "Fizz" is returned

        Examples:
            | input |
            | 3     |
            | 6     |
            | 9     |

    @e2e
    @debug
    Scenario Outline:: Number <input> is divisable by 5, return Buzz
        When the user enters <input>
        Then the word "Buzz" is returned

        Examples:
            | input |
            | 5     |
            | 10    |
            | 20    |
    @e2e
    @debug
    Scenario Outline:: Number <input> is divisable by 3 and 5, return FizzBuzz
        When the user enters <input>
        Then the word "FizzBuzz" is returned

        Examples:
            | input |
            | 15    |
            | 30    |
            | 45    |
    @e2e
    @debug
    Scenario Outline:: Any other Number <input>, return number
        When the user enters <input>
        Then the word "<input>" is returned

        Examples:
            | input |
            | 1     |
            | 2     |
            | 4     |