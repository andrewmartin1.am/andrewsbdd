const { When, Then } = require("cucumber");
const { expect } = require("chai");
const calculateResult = require("../../src/calculate-result");

// this where we store our result
//let result;

When("the user enters {int}", function(input) {
  //this is where we calculate whether its fizz or buzz etc.
  return this.result = calculateResult(input);
});

Then("the word {string} is returned", function(expectedResult) {
  // This is where we check the result is correct
  return expect(this.result).to.equal(expectedResult);
});

// alternate way of above using fat arrow
//Then('the word {string} is returned', function (expectedResult) => {
//       // Write code here that turns the phrase above into concrete actions
//        return 'pending';
//     });
