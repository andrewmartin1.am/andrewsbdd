function calculateResult(userInput) {
  if (userInput % 5 == 0 && userInput % 3 == 0) {
    return "FizzBuzz";
  }
  if (userInput % 3 == 0) {
    return "Fizz";
  }
  if (userInput % 5 == 0) {
    return "Buzz";
  }

  return userInput.toString();
}
module.exports = calculateResult;
