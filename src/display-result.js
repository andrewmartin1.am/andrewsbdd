function displayResult() {
  let input = document.querySelector("input#userInput").value;
  let result = calculateResult(input);
  document.querySelector("h1#result").innerHTML = result;
}
